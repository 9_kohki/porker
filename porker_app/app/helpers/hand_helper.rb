module HandHelper

  # エラー判定 ##############################################

  # class Validator
    # 未入力の場合
    def error_empty(cards)
      # cards.map do |card|

        if cards.blank?
          "5枚のカードを半角スペース区切りで入力してください。"
        end

      # end
    end

    # カードの枚数が不正
    def error_size(cards)
      cards.map do |card|
        card_array = card.split(/[\s]/)
        if card_array.size != 5
          "5枚のカードを半角スペース区切りで入力してください。"
        end
      end
    end

    # カードの重複
    def error_uniq(cards)
      cards.map do |card|
        card_array = card.split(" ")
        if card_array.uniq.size != card_array.size
          "カードが重複しています。"
        end
      end
    end

    # 不正なスートの検知
    def error_suit(cards)
      cards.map do |card|
        suits = card.scan(/[A-Z]|[a-z]/)
        if /\W/ === card.gsub(" ","")
          "カードが不正です。S,H,D,Cでスートを指定してください。"
        elsif (/[^SHDC]/ === suits.join)|(suits.join.size > 5)
          "カードが不正です。S,H,D,Cでスートを指定してください。"
        end
      end
    end


    # 不正な数字の検知（数字）
    def error_number(cards)
      cards.map do |card|
        numbers = card.scan(/\d{1,2}/)
        if numbers.any?{|n|(n.to_i==0)|(n.to_i==00)}
          "カードが不正です。1〜13で数字を指定してください。"
        elsif (/[1][4-9]/ === card)|(/[2-9][0-9]/ === card)|(/[0-9]{3,}/ === card)
          "カードが不正です。1〜13で数字を指定してください。"
        end
      end
    end


    # def error_order(cards)
    #   cards.map do |card|
    #     card_array = card.split(/[\s]/)
    #     if /^[\d]/ ===
    #       "スート＋数字の順番でカードを指定してください。"
    #     end
    #   end
    # end


    # # 小文字を検知
    # def error_case(cards)
    #   cards.map do |card|
    #     if /[a-z]/ === card
    #       "スートは大文字（S,H,D,C）で入力してください。"
    #     end
    #   end
    # end


    # 全角英数を検知
    def error_full(cards)
      cards.map do |card|
        if /[０-９]|[Ａ-Ｚ]/ === card
          "半角英数字で入力してください。"
        end
      end
    end


    # 不正な
    # # カードの枚数（スート）
    # def error_suitsize(cards)
    #   cards.map do |card|
    #     suits = card.scan(/[A-Z]/)
    #     if suits.size != 5
    #        "S,H,D,Cでスートを指定してください。"
    #     end
    #   end
    # end


    def all_error(cards)
      [
        # error_empty(cards),
        error_size(cards),
        error_uniq(cards),
        error_suit(cards),
        error_number(cards),
        # error_order(cards),
        # error_case(cards),
        error_full(cards),
        # error_suitsize(cards),
        # error_numbersize(cards)
      ]
    end
  # end



  # 役の判定 ########################################################

  # class HandChecker
    def check_hands(cards)
      cards.map do |card|

        suits = card.scan(/[A-Z]/)
        numbers = card.scan(/[1][0-3]|[1-9]/)

        if (numbers.map{|n|n.to_i}.sort == [1,10,11,12,13]) & (suits.uniq.size == 1)
          "ロイヤルストレートフラッシュ"
        elsif ((numbers.map{|n|n.to_i + 1} & numbers.map{|n|n.to_i}).size == 4) & (suits.uniq.size == 1)
          "ストレートフラッシュ"
        elsif suits.uniq.size == 1
          "フラッシュ"
        elsif (numbers.map{|n|n.to_i + 1} & numbers.map{|n|n.to_i}).size == 4
          "ストレート"
        elsif numbers.map{|n|n.to_i}.sort == [1,10,11,12,13]
          "ストレート"
        elsif numbers.uniq.size == 5
          "ハイカード"
        else

          case numbers.group_by(&:itself).map{|k,v|v.size}.sort
          when [1,4]
            "フォーカード"
          when [2,3]
            "フルハウス"
          when [1,1,3]
            "スリーカード"
          when [1,2,2]
            "ツーペア"
          when [1,1,1,2]
            "ワンペア"
          end

        end

      end
    end
  # end


  # 最良手の判定 #######################################################
  # class BestHandChecker
    def check_besthand(name)

      # 役とスコアの対応表
      hash = {"ロイヤルストレートフラッシュ"=>10,
           "ストレートフラッシュ"=>9,
           "フォーカード"=>8,
           "フルハウス"=>7,
           "フラッシュ"=>6,
           "ストレート"=>5,
           "スリーカード"=>4,
           "ツーペア"=>3,
           "ワンペア"=>2,
           "ハイカード"=>1}

      # 役をスコア化する
      scores = name.map{|n|n.gsub(/\W+/,hash)}

      # StringをIntegerに変換
      scores.map!{|score|score.to_i}

      # 配列中の最大値を抽出
      max_score = scores.max

      # スコア配列に対して最大値と等しい場合にtrueそうでない場合にfalseを返す
      scores.map!{|score| score == max_score}

    end
  # end

end
