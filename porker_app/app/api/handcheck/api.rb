require 'net/https'
require 'check_hand'
require 'check_besthand'
require 'validator'
require './app/services/difinition/http_status_code_difinition'
require './app/services/difinition/error_message_difinition'

include CheckHand
include CheckBestHand
include Validator
include HttpStatusCodeDefinition
include ErrorMessageDifinition

  module Handcheck
    class API < Grape::API

      format :json
      prefix :api

      helpers do
        def my_error!(message, status)
          error!({error: message}, status)
        end
      end

      resource :/ do

        post '/' do
          my_error!(INVALID_REQUEST_ERROR_MESSAGE, HTTP_STATUS_NOT_ALLOWED)
        end

      end

      resource :hand do

        post '/' do
          my_error!(INVALID_REQUEST_ERROR_MESSAGE, HTTP_STATUS_NOT_ALLOWED)
        end

        post '/check' do

          card = params[:cards]

          # 値が入っていない場合
          if card.blank?
            my_error!(EMPTY_ERROR_MESSAGE, HTTP_STATUS_BAD_REQUEST)

          # エラーメッセージが空でないなら
          elsif  all_error(card).join.present?

            # 参加人数
            players_count = (0..(card.size-1))

            # エラーメッセージを格納
            error_message = all_error(card)

            # messages_count = (0..(error_message.size-1))

            # それぞれのカードの組に対するエラーッセージ
            each_error = []
            players_count.each do |player_count|
              unless error_message[0][player_count].nil?
                each_error << "#{player_count+1}組目：#{error_message[0][player_count]}"
              end
              unless error_message[1][player_count].nil?
                each_error << "#{player_count+1}組目：#{error_message[1][player_count]}"
              end
              unless error_message[2][player_count].nil?
                each_error << "#{player_count+1}組目：#{error_message[2][player_count]}"
              end
              unless error_message[3][player_count].nil?
                each_error << "#{player_count+1}組目：#{error_message[3][player_count]}"
              end
              unless error_message[4][player_count].nil?
                each_error << "#{player_count+1}組目：#{error_message[4][player_count]}"
              end
              unless error_message[5][player_count].nil?
                each_error << "#{player_count+1}組目：#{error_message[5][player_count]}"
              end
            end

            my_error!(each_error, HTTP_STATUS_BAD_REQUEST)



          # エラーメッセージが空の場合
          else

            # 参加人数
            players_count = (0..(card.size - 1))

            hand = check_hand(card)

            best = check_besthand(hand)

            each_result = []

            players_count.each do |player_count|
              each_result << {
                "card"=> card[player_count],
                "hand"=> hand[player_count],
                "best"=> best[player_count]
              }
            end

            {"result": each_result}

          end
        end
      end
    end
  end











# get '/check' do
#   {
#     result: [
#               { card: "H1 H13 H12 H11 H10", hand: "ストレートフラッシュ", best: true },
#               { card: "H9 C9 S9 H2 C2", hand: "フルハウス", best: false },
#               { card: "C13 D12 C11 H8 H7", hand: "ハイカード", best: false }
#             ]
#   }
# end



#
# result = {
#   "result"=> [
#     {
#       "card"=> card[0],
#       "hand"=> hand[0],
#       "best"=> best[0]
#     },
#     {
#       "card"=> card[1],
#       "hand"=> hand[1],
#       "best"=> best[1]
#     },
#     {
#       "card"=> card[2],
#       "hand"=> hand[2],
#       "best"=> best[2]
#     }
#   ]
# }
