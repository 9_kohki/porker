class ApplicationController < ActionController::Base
  # Prevent CSFR attacks by raising an exception
  protect_from_forgery with: :exception

  rescue_from Exception, with: :handle_500 unless Rails.env.development?

  rescue_from ActionController::RoutingError, with: :handle_404 unless Rails.env.development?
  rescue_from ActiveRecord::RecordNotFound, with: :handle_404 unless Rails.env.development?

  def handle_500(exception = nil)
    logger.info "Rendering 500 with exception: #{exception.message}" if exception

    if request.xhr?
      render json: {error: '404 error'}, status:404
    else
      render template: 'errors/error_404', status:404, layout: 'applocation',content_type: 'text/html'
    end
  end

  def handle_404(exception = nil)
    logger.info "Rendering 404 with exception: #{exception.message}" if exception

    if request.xhr?
      # Ajaxのための処理
      render json: { error: '404 error' }, status: 404
    else
      render template: 'errors/error_404', status: 404, layout: 'application', content_type: 'text/html'
    end
  end



end

