
class HandController < ApplicationController
  #外部からのAPIを受ける特定アクションのみ除外
  protect_from_forgery :except => [:check]

    def top
    end

  include CheckHand
  include CheckBestHand
  include Validator


    def check

      @cards = params[:cards]

      # 受け取ったカードを配列に
      @cards_array = @cards.split(",")

      if error_empty(@cards_array).present?
        @empty_error = error_empty(@cards_array)
        render 'hand/top'

      # エラー判定が空であることを確認
      elsif all_error(@cards_array).join.present?
        @error_message = all_error(@cards_array)
        render 'hand/top'
      else

        # check_handsメソッドを使って役の名前を判定
        @hand_name = check_hand(@cards_array).join

        # @score = score(@hand_name)

        render 'hand/top'
      end

    end
end


