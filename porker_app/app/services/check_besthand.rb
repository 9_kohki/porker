require 'difinition/hand_name_difinition'

module CheckBestHand

  include HandNameDifinition

  def check_besthand(hand_name)

    # 役とスコアの対応表
    hash = {ROYAL_STRAIGHT_FLUSH=>10,
            STRAIGHT_FLUSH=>9,
            FOUR_OF_A_KIND=>8,
            FULL_HOUSE=>7,
            FLUSH=>6,
            STRAIGHT=>5,
            THREE_OF_A_KIND=>4,
            TWO_PAIR=>3,
            ONE_PAIR=>2,
            HIGH_CARD=>1}

    # 役をスコア化する
    scores = hand_name.map{|name|name.gsub(/\W+/,hash)}

    # StringをIntegerに変換
    scores.map!{|score|score.to_i}

    # 配列中の最大値を抽出
    max_score = scores.max

    # スコア配列に対して最大値と等しい場合にtrueそうでない場合にfalseを返す
    scores.map!{|score| score == max_score}

  end

end