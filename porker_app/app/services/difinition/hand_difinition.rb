module HandDifinition

  ROYAL_STRAIGHT_FLUSH_HAND = "S1 S10 S11 S12 S13"
  STRAIGHT_FLUSH_HAND       = "S1 S2 S3 S4 S5"
  FOUR_OF_A_KIND_HAND       = "S10 H10 D10 C10 S5"
  FULL_HOUSE_HAND           = "S1 H1 D1 C2 S2"
  FLUSH_HAND                = "H1 H3 H5 H7 H9"
  STRAIGHT_HAND             = "S1 H2 D3 C4 S5"
  THREE_OF_A_KIND_HAND      = "S10 H10 D10 C5 S1"
  TWO_PAIR_HAND             = "S10 H10 D5 C5 S1"
  ONE_PAIR_HAND             = "S10 H10 D1 C2 S3"
  HIGH_CARD_HAND            = "S1 H3 D5 C7 S9"

end
