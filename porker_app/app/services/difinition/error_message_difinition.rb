module ErrorMessageDifinition

  BASIC_ERROR_MESSAGE           = "5枚のカードを半角スペース区切りで入力してください。"
  DUPLICATE_ERROR_MESSAGE       = "カードが重複しています。"
  INVALID_SUIT_ERROR_MESSAGE    = "カードが不正です。S,H,D,Cでスートを指定してください。"
  INVALID_NUMBER_ERROR_MESSAGE  = "カードが不正です。1〜13で数字を指定してください。"
  INVALID_ORDER_ERROR_MESSAGE   = "スート(S,H,D,C)＋数字(1〜13)の順番でカードを指定してください。"
  INVALID_FORMAT_ERROR_MESSAGE  = "半角英数字で入力してください。"

  INVALID_REQUEST_ERROR_MESSAGE = "不正なリクエストです。"
  EMPTY_ERROR_MESSAGE           = "カードが未入力です。"

end
