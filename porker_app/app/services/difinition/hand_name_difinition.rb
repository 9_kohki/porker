module HandNameDifinition

  ROYAL_STRAIGHT_FLUSH = "ロイヤルストレートフラッシュ"
  STRAIGHT_FLUSH       = "ストレートフラッシュ"
  FOUR_OF_A_KIND       = "フォーカード"
  FULL_HOUSE           = "フルハウス"
  FLUSH                = "フラッシュ"
  STRAIGHT             = "ストレート"
  THREE_OF_A_KIND      = "スリーカード"
  TWO_PAIR             = "ツーペア"
  ONE_PAIR             = "ワンペア"
  HIGH_CARD            = "ハイカード"

end
