require 'difinition/error_message_difinition'

module Validator


  include ErrorMessageDifinition

  # 未入力の場合
  def error_empty(cards)
    if cards.blank?
      EMPTY_ERROR_MESSAGE
    end
  end


  private

  # カードの枚数が不正
  def error_size(hands_array)
    hands_array.map do |cards|
      cards_array = cards.split(/[\s]/)
      if cards_array.size != 5
        BASIC_ERROR_MESSAGE
      end
    end
  end

  # カードの重複
  def error_uniq(hands_array)
    hands_array.map do |cards|
      cards_array = cards.split(" ")
      if cards_array.uniq.size != cards_array.size
        DUPLICATE_ERROR_MESSAGE
      end
    end
  end

  # 不正なスートの検知
  def error_suit(hands_array)
    hands_array.map do |cards|
      suits = cards.scan(/[A-Z]|[a-z]/)
      if (/[^SHDC]/ === suits.join)|(/\W/ === cards.gsub(" ",""))
        INVALID_SUIT_ERROR_MESSAGE
      end
    end
  end

  # (suits.join.size > 5)|

  # 不正な数字の検知
  def error_number(hands_array)
    hands_array.map do |cards|
      numbers = cards.scan(/\d{1,2}/)
      if numbers.any?{|n|(n.to_i==0)|(n.to_i==00)}
        INVALID_NUMBER_ERROR_MESSAGE
      elsif (/[1][4-9]/ === cards)|(/[2-9][0-9]/ === cards)|(/[0-9]{3,}/ === cards)
        INVALID_NUMBER_ERROR_MESSAGE
      end
    end
  end


  def error_order(hands_array)
    hands_array.map do |cards|
      cards_array = cards.split(" ")
      if cards_array.any?{|card|/^[0-9]/ === card}
        INVALID_ORDER_ERROR_MESSAGE
      end
    end
  end



  # 全角英数を検知
  def error_full(hands_array)
    hands_array.map do |cards|
      if /[０-９]|[Ａ-Ｚ]/ === cards
        INVALID_FORMAT_ERROR_MESSAGE
      end
    end
  end



  # # 小文字を検知
  # def error_case(cards)
  #   cards.map do |card|
  #     if /[a-z]/ === card
  #       "スートは大文字（S,H,D,C）で入力してください。"
  #     end
  #   end
  # end

  # カードの枚数（スート）
  # def error_suitsize(cards)
  #   cards.map do |card|
  #     suits = card.scan(/[A-Z]/)
  #     if suits.size != 5
  #        "S,H,D,Cでスートを指定してください。"
  #     end
  #   end
  # end




  public

  def all_error(hands_array)
    [
      # error_empty(hands_array),
      error_size(hands_array),
      error_uniq(hands_array),
      error_suit(hands_array),
      error_number(hands_array),
      error_full(hands_array),
      error_order(hands_array),
      # error_case(hands_array),
      # error_suitsize(hands_array),
    ]
  end

end