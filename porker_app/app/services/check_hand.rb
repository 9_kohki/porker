require 'difinition/hand_name_difinition'

module CheckHand

  include HandNameDifinition





  def check_hand(cards_array)
    cards_array.map do |card|

      suits = card.scan(/[A-Z]/)
      numbers = card.scan(/[1][0-3]|[1-9]/)

      if (numbers.map{|n|n.to_i}.sort == [1,10,11,12,13]) & (suits.uniq.size == 1)
        ROYAL_STRAIGHT_FLUSH
      elsif ((numbers.map{|n|n.to_i + 1} & numbers.map{|n|n.to_i}).size == 4) & (suits.uniq.size == 1)
        STRAIGHT_FLUSH
      elsif suits.uniq.size == 1
        FLUSH
      elsif (numbers.map{|n|n.to_i + 1} & numbers.map{|n|n.to_i}).size == 4
        STRAIGHT
      elsif numbers.map{|n|n.to_i}.sort == [1,10,11,12,13]
        STRAIGHT
      elsif numbers.uniq.size == 5
        HIGH_CARD
      else

        case numbers.group_by(&:itself).map{|k,v|v.size}.sort
        when [1,4]
          FOUR_OF_A_KIND
        when [2,3]
          FULL_HOUSE
        when [1,1,3]
          THREE_OF_A_KIND
        when [1,2,2]
          TWO_PAIR
        when [1,1,1,2]
          ONE_PAIR
        end

      end

    end
  end


end