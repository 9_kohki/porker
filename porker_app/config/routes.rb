
Rails.application.routes.draw do

  get '/' => 'hand#top'
  get 'hand/check' => 'hand#top'
  post 'hand/check' => 'hand#check'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  mount Handcheck::API => '/'

  # namespace :API do
  #   resources :handcheck
  # end

  match '*path' => 'application#handle_404', via: :all

end
