require 'rails_helper'
require 'json'
require './app/services/difinition/hand_difinition'
require './app/services/difinition/hand_name_difinition'
require './app/services/difinition/error_message_difinition'

require './app/services/difinition/http_status_code_difinition'

include HandDifinition
include HandNameDifinition
include ErrorMessageDifinition

include HttpStatusCodeDefinition

RSpec.describe "HandcheckApi", type: :request do

  EMPTY_ERROR_HAND          = ""
  SIZE_ERROR_HAND           = "S1 S2 S3 S4 S5 S6"
  DUPLICATE_ERROR_HAND      = "S1 S1 S2 S3 S4"
  INVALID_SUIT_ERROR_HAND   = "A1 S1 D1 H1 C1"
  INVALID_NUMBER_ERROR_HAND = "S10 S11 S12 S13 S14"
  INVALID_ORDER_ERROR_HAND  = "1S 2S 3S 4S 5S"
  INVALID_FORMAT_ERROR_HAND = "Ｓ１　Ｓ２　Ｓ３　Ｓ４　Ｓ５"

  
  context 'カードの入力形式が正しい場合' do
    describe "POST /api/hand/check" do
      before do
        @hands = {
                    "cards":[
                              ROYAL_STRAIGHT_FLUSH_HAND,
                              STRAIGHT_FLUSH_HAND,
                              FOUR_OF_A_KIND_HAND,
                              FULL_HOUSE_HAND,
                              FLUSH_HAND,
                              STRAIGHT_HAND,
                              THREE_OF_A_KIND_HAND,
                              TWO_PAIR_HAND,
                              ONE_PAIR_HAND,
                              HIGH_CARD_HAND
                            ]
                 }

        post '/api/hand/check', params: @hands
        @json = response.body
        @expect_json = "{\"result\":[{\"card\":\"S1 S10 S11 S12 S13\",\"hand\":\"ロイヤルストレートフラッシュ\",\"best\":true},{\"card\":\"S1 S2 S3 S4 S5\",\"hand\":\"ストレートフラッシュ\",\"best\":false},{\"card\":\"S10 H10 D10 C10 S5\",\"hand\":\"フォーカード\",\"best\":false},{\"card\":\"S1 H1 D1 C2 S2\",\"hand\":\"フルハウス\",\"best\":false},{\"card\":\"H1 H3 H5 H7 H9\",\"hand\":\"フラッシュ\",\"best\":false},{\"card\":\"S1 H2 D3 C4 S5\",\"hand\":\"ストレート\",\"best\":false},{\"card\":\"S10 H10 D10 C5 S1\",\"hand\":\"スリーカード\",\"best\":false},{\"card\":\"S10 H10 D5 C5 S1\",\"hand\":\"ツーペア\",\"best\":false},{\"card\":\"S10 H10 D1 C2 S3\",\"hand\":\"ワンペア\",\"best\":false},{\"card\":\"S1 H3 D5 C7 S9\",\"hand\":\"ハイカード\",\"best\":false}]}"      end

      it "リクエストに対して201 Createdが返ってくる" do
        expect(response.status).to eq HTTP_STATUS_CREATED
      end

      it "期待されるJSON形式の判定結果が返っていくること" do
        expect(@json).to eq @expect_json
      end
    end
  end


  context 'カードの入力形式が誤っている場合' do
    describe "POST /api/hand/check" do
      before do
        @hands = {
                    "cards":[
                              EMPTY_ERROR_HAND,
                              SIZE_ERROR_HAND,
                              DUPLICATE_ERROR_HAND,
                              INVALID_SUIT_ERROR_HAND,
                              INVALID_NUMBER_ERROR_HAND,
                              INVALID_ORDER_ERROR_HAND,
                              INVALID_FORMAT_ERROR_HAND
                            ]
                 }

        post '/api/hand/check', params: @hands
        @json = response.body
        @expect_error_message = "{\"error\":[\"1組目：5枚のカードを半角スペース区切りで入力してください。\",\"2組目：5枚のカードを半角スペース区切りで入力してください。\",\"3組目：カードが重複しています。\",\"4組目：カードが不正です。S,H,D,Cでスートを指定してください。\",\"5組目：カードが不正です。1〜13で数字を指定してください。\",\"6組目：スート(S,H,D,C)＋数字(1〜13)の順番でカードを指定してください。\",\"7組目：5枚のカードを半角スペース区切りで入力してください。\",\"7組目：カードが不正です。S,H,D,Cでスートを指定してください。\",\"7組目：半角英数字で入力してください。\"]}"
      end

      it "リクエストに対して400 Bad Requestが返ってくる" do
        expect(response.status).to eq HTTP_STATUS_BAD_REQUEST
      end

      it "期待されるJSON形式のエラーメッセージが返ってくること" do
        expect(@json).to eq @expect_error_message
      end
    end
  end


  context 'カードが未入力の場合' do
    describe 'POST api/hand/check' do
      before do
        @hands = ""
        post '/api/hand/check', params: @hands
        @json = response.body
        @expect_error_message = "{\"error\":\"カードが未入力です。\"}"

      end

      it "リクエストに対して400 Bad Requestが返ってくる" do
        expect(response.status).to eq HTTP_STATUS_BAD_REQUEST
      end

      it "期待されるJSON形式のエラーメッセージが返ってくること" do
        expect(@json).to eq @expect_error_message
      end

    end
  end


  context 'URLが間違っている場合' do
    describe 'POST api/' do
      before do
        post '/api'
        @json = response.body
        @expect_error_message = "{\"error\":\"不正なリクエストです。\"}"

      end

      it "リクエストに対して405 Not Allowedが返ってくる" do
        expect(response.status).to eq HTTP_STATUS_NOT_ALLOWED
      end

      it "期待されるJSON形式のエラーメッセージが返ってくること" do
        expect(@json).to eq @expect_error_message
      end

    end

    # describe 'GET api/' do
    #   before do
    #     get '/api'
    #     @json = response.body
    #     @expect_error_message = "{\"error\":\"405 Not Allowed\"}"
    #
    #   end
    #
    #   it "リクエストに対して405 Not Allowedが返ってくる" do
    #     expect(response.status).to eq(405)
    #   end
    #
    #   it "期待されるJSON形式のエラーメッセージが返ってくること" do
    #     expect(@json).to eq @expect_error_message
    #   end
    #
    # end

    describe 'POST api/hand/' do
      before do
        post '/api/hand'
        @json = response.body
        @expect_error_message = "{\"error\":\"不正なリクエストです。\"}"

      end

      it "リクエストに対して405 Not Allowedが返ってくる" do
        expect(response.status).to eq HTTP_STATUS_NOT_ALLOWED
      end

      it "期待されるJSON形式のエラーメッセージが返ってくること" do
        expect(@json).to eq @expect_error_message
      end

    end

    # describe 'GET api/hand' do
    #   before do
    #     get '/api/hand'
    #     @json = response.body
    #     @expect_error_message = "{\"error\":\"405 Not Allowed\"}"
    #
    #   end
    #
    #   it "リクエストに対して201 Createdが返ってくる" do
    #     expect(response.status).to eq(405)
    #   end
    #
    #   it "期待されるJSON形式のエラーメッセージが返ってくること" do
    #     expect(@json).to eq @expect_error_message
    #   end
    #
    # end

    # describe 'GET api/hand/check' do
    #   before do
    #     get '/api/hand/check'
    #     @json = response.body
    #     @expect_error_message = "{\"error\":\"405 Not Allowed\"}"
    #
    #   end
    #
    #   it "リクエストに対して405 Not Allowedが返ってくる" do
    #     expect(response.status).to eq(405)
    #   end
    #
    #   it "期待されるJSON形式のエラーメッセージが返ってくること" do
    #     expect(@json).to eq @expect_error_message
    #   end
    #
    # end
  end


end












#
# [
#   "H1 H13 H12 H11 H10",
#   "H9 C9 S9 H2 C2",
#   "C13 D12 C11 H8 H7"
# ]

# {
#   "result": [
#               {
#                 "card": "H1 H13 H12 H11 H10",
#                 "hand": "ストレートフラッシュ",
#                 "best": true
#               },
#               { "card": "H9 C9 S9 H2 C2",
#                 "hand": "フルハウス",
#                 "best": false
#               },
#               {
#                 "card": "C13 D12 C11 H8 H7",
#                 "hand": "ハイカード",
#                 "best": false
#               }
#             ]
# }


# handcheck_api_path
#
#
#
# {
#   "cards":
#   [
#    "H1 H13 H12 H11 H10",
#    "H9 C9 S9 H2 C2",
#    "C13 D12 C11 H8 H7",
#    "S1 S10 S11 S12 S13"
#   ]
# }
