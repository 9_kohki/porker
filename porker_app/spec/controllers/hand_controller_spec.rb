require 'rails_helper'
require './app/services/difinition/hand_difinition'
require './app/services/difinition/hand_name_difinition'
require './app/services/difinition/error_message_difinition'

include HandDifinition
include HandNameDifinition
include ErrorMessageDifinition



RSpec.describe HandController, type: :controller do


  # topアクション #########################################

  context '更新処理' do
    describe 'GET #top' do
      before do
        get :top
      end
      it 'リクエストが200 OKとなること' do
        expect(response.status).to eq 200
      end
      it ':topテンプレートを表示すること' do
        expect(response).to render_template :top
      end
    end
  end




  # checkアクション #######################################

  context '@error_messageが空のとき' do

    describe 'POST #check' do
      before do
        @cards = {cards: ROYAL_STRAIGHT_FLUSH_HAND}
        post :check, params: @cards
        @cards_array = [ROYAL_STRAIGHT_FLUSH_HAND]
        @hand_name = ROYAL_STRAIGHT_FLUSH
      end

      it 'リクエストが200 OKであること' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている' do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq nil
      end

      it '@hand_nameに期待される値が入っている' do
        expect(assigns(:hand_name)).to eq @hand_name
      end

      it ':topテンプレートが表示されること' do
        expect(response).to render_template :top
      end

    end

    describe 'POST #check' do
      before do
        @cards = {cards: STRAIGHT_FLUSH_HAND}
        post :check, params: @cards
        @cards_array = [STRAIGHT_FLUSH_HAND]
        @hand_name = STRAIGHT_FLUSH
      end

      it 'リクエストが200 OKであること' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている' do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq nil
      end

      it '@hand_nameに期待される値が入っている' do
        expect(assigns(:hand_name)).to eq @hand_name
      end

      it ':topテンプレートが表示されること' do
        expect(response).to render_template :top
      end

    end

    describe 'POST #check' do
      before do
        @cards = {cards: FLUSH_HAND}
        post :check, params: @cards
        @cards_array = [FLUSH_HAND]
        @hand_name = FLUSH
      end

      it 'リクエストが200 OKであること' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている' do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq nil
      end

      it '@hand_nameに期待される値が入っている' do
        expect(assigns(:hand_name)).to eq @hand_name
      end

      it ':topテンプレートが表示されること' do
        expect(response).to render_template :top
      end

    end

    describe 'POST #check' do
      before do
        @cards = {cards: STRAIGHT_HAND}
        post :check, params: @cards
        @cards_array = [STRAIGHT_HAND]
        @hand_name = STRAIGHT
      end

      it 'リクエストが200 OKであること' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている' do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq nil
      end

      it '@hand_nameに期待される値が入っている' do
        expect(assigns(:hand_name)).to eq @hand_name
      end

      it ':topテンプレートが表示されること' do
        expect(response).to render_template :top
      end

    end

    describe 'POST #check' do
      before do
        @cards = {cards: HIGH_CARD_HAND}
        post :check, params: @cards
        @cards_array = [HIGH_CARD_HAND]
        @hand_name = HIGH_CARD
      end

      it 'リクエストが200 OKであること' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている' do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq nil
      end

      it '@hand_nameに期待される値が入っている' do
        expect(assigns(:hand_name)).to eq @hand_name
      end

      it ':topテンプレートが表示されること' do
        expect(response).to render_template :top
      end

    end


    describe 'POST #check' do
      before do
        @cards = {cards: FOUR_OF_A_KIND_HAND}
        post :check, params: @cards
        @cards_array = [FOUR_OF_A_KIND_HAND]
        @hand_name = FOUR_OF_A_KIND
      end

      it 'リクエストが200 OKであること' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている' do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq nil
      end

      it '@hand_nameに期待される値が入っている' do
        expect(assigns(:hand_name)).to eq @hand_name
      end

      it ':topテンプレートが表示されること' do
        expect(response).to render_template :top
      end

    end

    describe 'POST #check' do
      before do
        @cards = {cards: FULL_HOUSE_HAND}
        post :check, params: @cards
        @cards_array = [FULL_HOUSE_HAND]
        @hand_name = FULL_HOUSE
      end

      it 'リクエストが200 OKであること' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている' do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq nil
      end

      it '@hand_nameに期待される値が入っている' do
        expect(assigns(:hand_name)).to eq @hand_name
      end

      it ':topテンプレートが表示されること' do
        expect(response).to render_template :top
      end

    end

    describe 'POST #check' do
      before do
        @cards = {cards: THREE_OF_A_KIND_HAND}
        post :check, params: @cards
        @cards_array = [THREE_OF_A_KIND_HAND]
        @hand_name = THREE_OF_A_KIND
      end

      it 'リクエストが200 OKであること' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている' do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq nil
      end

      it '@hand_nameに期待される値が入っている' do
        expect(assigns(:hand_name)).to eq @hand_name
      end

      it ':topテンプレートが表示されること' do
        expect(response).to render_template :top
      end

    end


    describe 'POST #check' do
      before do
        @cards = {cards: TWO_PAIR_HAND}
        post :check, params: @cards
        @cards_array = [TWO_PAIR_HAND]
        @hand_name = TWO_PAIR
      end

      it 'リクエストが200 OKであること' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている' do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq nil
      end

      it '@hand_nameに期待される値が入っている' do
        expect(assigns(:hand_name)).to eq @hand_name
      end

      it ':topテンプレートが表示されること' do
        expect(response).to render_template :top
      end

    end

    describe 'POST #check' do
      before do
        @cards = {cards: ONE_PAIR_HAND}
        post :check, params: @cards
        @cards_array = [ONE_PAIR_HAND]
        @hand_name = ONE_PAIR
      end

      it 'リクエストが200 OKであること' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている' do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq nil
      end

      it '@hand_nameに期待される値が入っている' do
        expect(assigns(:hand_name)).to eq @hand_name
      end

      it ':topテンプレートが表示されること' do
        expect(response).to render_template :top
      end

    end


  end



  context '@error_messageに値が入っている時' do

    describe 'POST #check' do
      before do
        @cards = {cards: EMPTY_ERROR_HAND}
        post :check, params: @cards
        @cards_array = []
        @error_message = EMPTY_ERROR_MESSAGE

      end
      it 'リクエストが200 OKである' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている'do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:empty_error)).to eq @error_message
      end

      it ':topテンプレートが表示される' do
        expect(response).to render_template :top
      end

    end

    describe 'POST #check' do
      before do
        @cards = {cards: SIZE_ERROR_HAND}
        post :check, params: @cards
        @cards_array = [SIZE_ERROR_HAND]
        @error_message = [[BASIC_ERROR_MESSAGE],
                          [nil],
                          [nil],
                          [nil],
                          [nil],
                          [nil]]
      end
      it 'リクエストが200 OKである' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている'do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq @error_message
      end

      it ':topテンプレートが表示される' do
        expect(response).to render_template :top
      end

    end

    describe 'POST #check' do
      before do
        @cards = {cards: DUPLICATE_ERROR_HAND}
        post :check, params: @cards
        @cards_array = [DUPLICATE_ERROR_HAND]
        @error_message = [[nil],
                          [DUPLICATE_ERROR_MESSAGE],
                          [nil],
                          [nil],
                          [nil],
                          [nil]]
      end


      it 'リクエストが200 OKである' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている'do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq @error_message
      end

      it ':topテンプレートが表示される' do
        expect(response).to render_template :top
      end

    end

    describe 'POST #check' do
      before do
        @cards = {cards: INVALID_SUIT_ERROR_HAND}
        post :check, params: @cards
        @cards_array = [INVALID_SUIT_ERROR_HAND]
        @error_message = [[nil],
                          [nil],
                          [INVALID_SUIT_ERROR_MESSAGE],
                          [nil],
                          [nil],
                          [nil]]

      end
      it 'リクエストが200 OKである' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている'do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq @error_message
      end

      it ':topテンプレートが表示される' do
        expect(response).to render_template :top
      end

    end

    describe 'POST #check' do
      before do
        @cards = {cards: INVALID_NUMBER_ERROR_HAND}
        post :check, params: @cards
        @cards_array = [INVALID_NUMBER_ERROR_HAND]
        @error_message = [[nil],
                          [nil],
                          [nil],
                          [INVALID_NUMBER_ERROR_MESSAGE],
                          [nil],
                          [nil]]

      end
      it 'リクエストが200 OKである' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている'do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq @error_message
      end

      it ':topテンプレートが表示される' do
        expect(response).to render_template :top
      end

    end

    # describe 'POST #check' do
    #   before do
    #     @cards = {cards: "s1 h2 d3 c4 s2"}
    #     post :check, params: @cards
    #     @cards_array = ["s1 h2 d3 c4 s2"]
    #     @error_message = [[nil], [nil], [nil], [nil], ["スートは大文字（S,H,D,C）で入力してください。"], [nil]]
    #
    #   end
    #   it 'リクエストが200 OKである' do
    #     expect(response.status).to eq 200
    #   end
    #
    #   it '@cards_arrayに期待される配列が入っている'do
    #     expect(assigns(:cards_array)).to eq @cards_array
    #   end
    #
    #   it '@error_messageに期待される値が入っている' do
    #     expect(assigns(:error_message)).to eq @error_message
    #   end
    #
    #   it ':topテンプレートが表示される' do
    #     expect(response).to render_template :top
    #   end
    #
    # end

    describe 'POST #check' do
      before do
        @cards = {cards: INVALID_FORMAT_ERROR_HAND}
        post :check, params: @cards
        @cards_array = [INVALID_FORMAT_ERROR_HAND]
        @error_message = [[BASIC_ERROR_MESSAGE],
                          [nil],
                          [INVALID_SUIT_ERROR_MESSAGE],
                          [nil],
                          [INVALID_FORMAT_ERROR_MESSAGE],
                          [nil]]

      end
      it 'リクエストが200 OKである' do
        expect(response.status).to eq 200
      end

      it '@cards_arrayに期待される配列が入っている'do
        expect(assigns(:cards_array)).to eq @cards_array
      end

      it '@error_messageに期待される値が入っている' do
        expect(assigns(:error_message)).to eq @error_message
      end

      it ':topテンプレートが表示される' do
        expect(response).to render_template :top
      end

    end

  end

end

