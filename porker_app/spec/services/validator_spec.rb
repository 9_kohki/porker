require 'rails_helper'
require './app/services/difinition/error_message_difinition'

include ErrorMessageDifinition


RSpec.describe Validator do

  EMPTY_ERROR_HAND          = ""
  SIZE_ERROR_HAND           = "S1 S2 S3 S4 S5 S6"
  DUPLICATE_ERROR_HAND      = "S1 S1 S2 S3 S4"
  INVALID_SUIT_ERROR_HAND   = "A1 S1 D1 H1 C1"
  INVALID_NUMBER_ERROR_HAND = "S10 S11 S12 S13 S14"
  INVALID_ORDER_ERROR_HAND  = "1S 2S 3S 4S 5S"
  INVALID_FORMAT_ERROR_HAND = "Ｓ１　Ｓ２　Ｓ３　Ｓ４　Ｓ５"

  it '未入力の場合' do
    expect(error_empty(EMPTY_ERROR_HAND)).to eq EMPTY_ERROR_MESSAGE
  end

  it 'カードの枚数が不正' do
    expect(error_size([SIZE_ERROR_HAND])).to eq [BASIC_ERROR_MESSAGE]
  end

  it 'カードの重複' do
    expect(error_uniq([DUPLICATE_ERROR_HAND])).to eq [DUPLICATE_ERROR_MESSAGE]
  end

  it '不正なスートの検知' do
    expect(error_suit([INVALID_SUIT_ERROR_HAND])).to eq [INVALID_SUIT_ERROR_MESSAGE]
  end

  it '不正な数字の検知' do
    expect(error_number([INVALID_NUMBER_ERROR_HAND])).to eq [INVALID_NUMBER_ERROR_MESSAGE]
  end

  it '不正な順番の検知' do
    expect(error_order([INVALID_ORDER_ERROR_HAND])).to eq [INVALID_ORDER_ERROR_MESSAGE]
  end

  it '全角英数の検知' do
    expect(error_full([INVALID_FORMAT_ERROR_HAND])).to eq [INVALID_FORMAT_ERROR_MESSAGE]
  end

end
