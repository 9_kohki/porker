require 'rails_helper'
require './app/services/difinition/hand_difinition'
require './app/services/difinition/hand_name_difinition'

include HandDifinition
include HandNameDifinition

RSpec.describe CheckHand do

    it 'ロイヤルストレートフラッシュ' do
      expect(check_hand([ROYAL_STRAIGHT_FLUSH_HAND])).to eq [ROYAL_STRAIGHT_FLUSH]
    end

    it 'ストレートフラッシュ' do
      expect(check_hand([STRAIGHT_FLUSH_HAND])).to eq [STRAIGHT_FLUSH]
    end

    it 'フォーカード' do
      expect(check_hand([FOUR_OF_A_KIND_HAND])).to eq [FOUR_OF_A_KIND]
    end

    it 'フルハウス' do
      expect(check_hand([FULL_HOUSE_HAND])).to eq [FULL_HOUSE]
    end

    it 'フラッシュ' do
      expect(check_hand([FLUSH_HAND])).to eq [FLUSH]
    end

    it 'ストレート' do
      expect(check_hand([STRAIGHT_HAND])).to eq [STRAIGHT]
    end

    it 'スリーカード' do
      expect(check_hand([THREE_OF_A_KIND_HAND])).to eq [THREE_OF_A_KIND]
    end

    it 'ツーペア' do
      expect(check_hand([TWO_PAIR_HAND])).to eq [TWO_PAIR]
    end

    it 'ワンペア' do
      expect(check_hand([ONE_PAIR_HAND])).to eq [ONE_PAIR]
    end

    it 'ハイカード' do
      expect(check_hand([HIGH_CARD_HAND])).to eq [HIGH_CARD]
    end


    # it '' do
    #   expect(suits).to eq @suits
    # end
    #
    # it '' do
    #   expect(numbers).to eq @numbers
    # end

end


