require 'rails_helper'
require './app/services/difinition/hand_name_difinition'

include HandNameDifinition

RSpec.describe CheckBestHand do

  describe 'ロイヤルストレートフラッシュが最良手として判定される' do

    before do
      @hand_name_array = ROYAL_STRAIGHT_FLUSH,
                        STRAIGHT_FLUSH,
                        FOUR_OF_A_KIND,
                        FULL_HOUSE,
                        FLUSH,
                        STRAIGHT,
                        THREE_OF_A_KIND,
                        TWO_PAIR,
                        ONE_PAIR,
                        HIGH_CARD

      @best_hand_array = true,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false
    end

    it 'ベストスコアにtrueを、そうでないスコアに対してfalseを返すこと' do
      expect(check_besthand(@hand_name_array)).to eq @best_hand_array
    end

  end

  describe 'ストレートフラッシュが最良手として判定される' do

    before do
      @hand_name_array = STRAIGHT_FLUSH,
                        STRAIGHT_FLUSH,
                        FOUR_OF_A_KIND,
                        FULL_HOUSE,
                        FLUSH,
                        STRAIGHT,
                        THREE_OF_A_KIND,
                        TWO_PAIR,
                        ONE_PAIR,
                        HIGH_CARD

      @best_hand_array = true,
                        true,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false
    end

    it 'ベストスコアにtrueを、そうでないスコアに対してfalseを返すこと' do
      expect(check_besthand(@hand_name_array)).to eq @best_hand_array
    end

  end

  describe 'フォーカードが最良手として判定される' do

    before do
      @hand_name_array = FOUR_OF_A_KIND,
                        FOUR_OF_A_KIND,
                        FOUR_OF_A_KIND,
                        FULL_HOUSE,
                        FLUSH,
                        STRAIGHT,
                        THREE_OF_A_KIND,
                        TWO_PAIR,
                        ONE_PAIR,
                        HIGH_CARD

      @best_hand_array = true,
                        true,
                        true,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false
    end

    it 'ベストスコアにtrueを、そうでないスコアに対してfalseを返すこと' do
      expect(check_besthand(@hand_name_array)).to eq @best_hand_array
    end

  end

  describe 'フルハウスが最良手として判定される' do

    before do
      @hand_name_array = FULL_HOUSE,
                        FULL_HOUSE,
                        FULL_HOUSE,
                        FULL_HOUSE,
                        FLUSH,
                        STRAIGHT,
                        THREE_OF_A_KIND,
                        TWO_PAIR,
                        ONE_PAIR,
                        HIGH_CARD

      @best_hand_array = true,
                        true,
                        true,
                        true,
                        false,
                        false,
                        false,
                        false,
                        false,
                        false
    end

    it 'ベストスコアにtrueを、そうでないスコアに対してfalseを返すこと' do
      expect(check_besthand(@hand_name_array)).to eq @best_hand_array
    end

  end

  describe 'フラッシュが最良手として判定される' do

    before do
      @hand_name_array = FLUSH,
                        FLUSH,
                        FLUSH,
                        FLUSH,
                        FLUSH,
                        STRAIGHT,
                        THREE_OF_A_KIND,
                        TWO_PAIR,
                        ONE_PAIR,
                        HIGH_CARD

      @best_hand_array = true,
                        true,
                        true,
                        true,
                        true,
                        false,
                        false,
                        false,
                        false,
                        false
    end

    it 'ベストスコアにtrueを、そうでないスコアに対してfalseを返すこと' do
      expect(check_besthand(@hand_name_array)).to eq @best_hand_array
    end

  end

  describe 'ストレートが最良手として判定される' do

    before do
      @hand_name_array = STRAIGHT,
                        STRAIGHT,
                        STRAIGHT,
                        STRAIGHT,
                        STRAIGHT,
                        STRAIGHT,
                        THREE_OF_A_KIND,
                        TWO_PAIR,
                        ONE_PAIR,
                        HIGH_CARD

      @best_hand_array = true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        false,
                        false,
                        false,
                        false
    end

    it 'ベストスコアにtrueを、そうでないスコアに対してfalseを返すこと' do
      expect(check_besthand(@hand_name_array)).to eq @best_hand_array
    end

  end

  describe 'スリーカードが最良手として判定される' do

    before do
      @hand_name_array = THREE_OF_A_KIND,
                        THREE_OF_A_KIND,
                        THREE_OF_A_KIND,
                        THREE_OF_A_KIND,
                        THREE_OF_A_KIND,
                        THREE_OF_A_KIND,
                        THREE_OF_A_KIND,
                        TWO_PAIR,
                        ONE_PAIR,
                        HIGH_CARD

      @best_hand_array = true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        false,
                        false,
                        false
    end

    it 'ベストスコアにtrueを、そうでないスコアに対してfalseを返すこと' do
      expect(check_besthand(@hand_name_array)).to eq @best_hand_array
    end

  end

  describe 'ツーペアが最良手として判定される' do

    before do
      @hand_name_array = TWO_PAIR,
                        TWO_PAIR,
                        TWO_PAIR,
                        TWO_PAIR,
                        TWO_PAIR,
                        TWO_PAIR,
                        TWO_PAIR,
                        TWO_PAIR,
                        ONE_PAIR,
                        HIGH_CARD

      @best_hand_array = true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        false,
                        false
    end

    it 'ベストスコアにtrueを、そうでないスコアに対してfalseを返すこと' do
      expect(check_besthand(@hand_name_array)).to eq @best_hand_array
    end

  end

  describe 'ワンペアが最良手として判定される' do

    before do
      @hand_name_array = ONE_PAIR,
                        ONE_PAIR,
                        ONE_PAIR,
                        ONE_PAIR,
                        ONE_PAIR,
                        ONE_PAIR,
                        ONE_PAIR,
                        ONE_PAIR,
                        ONE_PAIR,
                        HIGH_CARD

      @best_hand_array = true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        false
    end

    it 'ベストスコアにtrueを、そうでないスコアに対してfalseを返すこと' do
      expect(check_besthand(@hand_name_array)).to eq @best_hand_array
    end

  end

  describe 'ハイカードが最良手として判定される' do

    before do
      @hand_name_array = HIGH_CARD,
                        HIGH_CARD,
                        HIGH_CARD,
                        HIGH_CARD,
                        HIGH_CARD,
                        HIGH_CARD,
                        HIGH_CARD,
                        HIGH_CARD,
                        HIGH_CARD,
                        HIGH_CARD

      @best_hand_array = true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true,
                        true
    end

    it 'ベストスコアにtrueを、そうでないスコアに対してfalseを返すこと' do
      expect(check_besthand(@hand_name_array)).to eq @best_hand_array
    end

  end



end
